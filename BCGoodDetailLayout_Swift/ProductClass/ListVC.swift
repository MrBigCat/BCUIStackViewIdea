//
//  ListVC.swift
//  BCGoodDetailLayout_Swift
//
//  Created by NewUser on 2018/5/22.
//  Copyright © 2018年 Shine. All rights reserved.
//

import UIKit

class ListVC: HomeBaseVC {

    var tab: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.brown
        tab = UITableView(frame: self.view.bounds, style: .plain)
        tab?.dataSource = self
        tab?.delegate   = self
        tab?.rowHeight = 50
        tab?.tableFooterView = UIView()
        self.view.addSubview(tab!)
    }

    deinit {
        print("释放")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        triggerDelegate()
    }
    
    fileprivate func triggerDelegate() {
        if let delegate = homeDelegate {
            delegate.upDataContains(self, (tab?.contentSize.height)! + 60)
            print(tab?.contentSize.height ?? 0)
        }
    }
    
}

extension ListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        cell?.textLabel?.text = "列表单元格\(indexPath.row)"
        cell?.contentView.backgroundColor = UIColor.brown
        return cell!
    }
    
    
}
