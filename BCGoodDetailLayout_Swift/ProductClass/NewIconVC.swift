//
//  NewIconVC.swift
//  BCGoodDetailLayout_Swift
//
//  Created by NewUser on 2018/5/22.
//  Copyright © 2018年 Shine. All rights reserved.
//

import UIKit

class NewIconVC: HomeBaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.purple
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let delegate = homeDelegate {
            delegate.upDataContains(self, 300.0)
        }
    }

   

}
