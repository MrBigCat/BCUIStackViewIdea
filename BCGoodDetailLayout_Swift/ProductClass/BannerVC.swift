//
//  BannerVC.swift
//  BCGoodDetailLayout_Swift
//
//  Created by NewUser on 2018/5/22.
//  Copyright © 2018年 Shine. All rights reserved.
//

import UIKit

class BannerVC: HomeBaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.red
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let delegate = homeDelegate {
            delegate.upDataContains(self, 200.0)
        }
    }
    

}
