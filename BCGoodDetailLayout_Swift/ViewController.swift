//
//  ViewController.swift
//  BCGoodDetailLayout_Swift
//
//  Created by NewUser on 2018/5/21.
//  Copyright © 2018年 Shine. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var scrollView: UIScrollView?
    var stackView: UIStackView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor  = UIColor.white
        setUI()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView?.contentSize = CGSize(width: (stackView?.frame.width)!, height: (stackView?.frame.height)!)
        print(scrollView?.contentSize.height ?? 0)
    }
    
}

extension ViewController {
    
    fileprivate func setUI() {
        scrollView = UIScrollView()
        view.addSubview(scrollView!)
        scrollView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.view)
        })
        // 刷新
        scrollView?.mj_header = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(ViewController.loadData))
        scrollView?.mj_footer = MJRefreshBackFooter(refreshingTarget: self, refreshingAction: #selector(ViewController.loadMoreData))
        
        stackView = UIStackView()
        stackView?.axis = .vertical
        stackView?.spacing = 10
        scrollView?.addSubview(stackView!)
        stackView?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.scrollView!)
            make.left.equalTo(self.scrollView!)
            make.right.equalTo(self.scrollView!)
            make.width.equalTo(self.scrollView!)
            make.bottom.equalTo(self.scrollView!)
        })
        
        setChildVC()
    }
    
    // 控制器
    fileprivate func setChildVC() {
        addVC(BannerVC())
        addVC(NewIconVC())
        addVC(AudioVC())
        addVC(VideoVC())
        addVC(ListVC())
    }
    // 加载控制器
    fileprivate func addVC(_ vc: HomeBaseVC) {
        self.addChildViewController(vc)
        self.stackView!.addArrangedSubview(vc.view)//将vc.view放于stackView容器中，此后不需要设置vc.view的任何约束，就能实现从上到下排列显示，宽高都是自适应大小，注意这儿必须要用addArrangedSubview而不能用addSubView

        vc.homeDelegate = self
    }
    
    // 模拟刷新
    @objc fileprivate func loadData() {
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 2) {
            DispatchQueue.main.async {
                self.scrollView?.mj_header.endRefreshing()
                //清空stackView中的 subView
                if let arrangedSubviews = self.stackView?.arrangedSubviews {
                    for subView in arrangedSubviews {
                        self.stackView?.removeArrangedSubview(subView)
                    }
                }
                // 删除子控制器
                for childVC in self.childViewControllers {
                    childVC.removeFromParentViewController()
                }
                
                // 重新加载
                self.setChildVC()
                
            }
        }
    }
    // 模拟加载更多
    @objc fileprivate func loadMoreData() {
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 2) {
            DispatchQueue.main.async {
                self.scrollView?.mj_footer.endRefreshing()
                self.scrollView?.mj_footer.endRefreshingWithNoMoreData()
                self.addVC(ListVC())
            }
        }
    }
}

extension ViewController: UPdataContainsDelegate {
    // 根据控制器回调设置约束高度
    func upDataContains( _ childVC: HomeBaseVC, _ containHeight: CGFloat) {
        // childVC.view.frame = CGRect(x: 0, y: 0, width: kSCREEN_WIDTH, height: 200) //不用设FRAME了，设了也不会起作用的，因为stackView会自动去添加一些约束。
        childVC.view.snp.makeConstraints { (make) in
            make.height.equalTo(containHeight)
        }
    }

}
